package com.keeps.googlekeep.Repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.keeps.googlekeep.model.Keep;

public interface KeepRepo extends MongoRepository<Keep, String> {

}
