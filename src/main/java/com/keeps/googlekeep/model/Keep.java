package com.keeps.googlekeep.model;

import javax.validation.constraints.NotNull;
import org.springframework.data.annotation.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Keep {
    @Id
    String id;
    @NotNull(message = "Title is required field.")
    String title;
    String category;
    String content;
}
