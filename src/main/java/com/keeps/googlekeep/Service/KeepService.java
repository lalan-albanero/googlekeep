package com.keeps.googlekeep.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.keeps.googlekeep.Exception.NotFoundException;
import com.keeps.googlekeep.Repository.KeepRepo;
import com.keeps.googlekeep.model.Keep;

import java.util.logging.Logger;

@Service
public class KeepService {
    final static Logger logger = Logger.getLogger(KeepService.class.getName());

    @Autowired
    KeepRepo db;

    public Keep addKeeps(Keep k1) {
        logger.info("Service :- In addKeeps method!!!");
        return db.save(k1);
    }

    public Optional<Keep> getKeepById(String id) throws NotFoundException {
        logger.info("Service :- In getKeepById method!!!");
        Optional<Keep> keep= db.findById(id);
        if(keep.isEmpty()){
            throw new NotFoundException("keep not found");
        }else{

            return keep;
        }
    }

    public List<Keep> getKeeps() throws NotFoundException {
        logger.info("Service :- In getKeeps method!!!");
        List<Keep> keep= db.findAll();
        if(keep.isEmpty()){
            throw new NotFoundException(" No keep found...");
        }else{
            return keep;
        }
    }

    public Keep updateKeepById(String id, Keep k2) {
        logger.info("Service :- In updateKeepById method!!!");
        k2.setId(id);
        return db.save(k2);
    }

    public String deleteById(String id) {
        logger.info("Service :- In deleteById method!!!");
        db.deleteById(id);
        return "Deleted Successfully....";
    }

}
