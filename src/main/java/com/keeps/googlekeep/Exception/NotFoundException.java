package com.keeps.googlekeep.Exception;

public class NotFoundException extends Exception {

    public NotFoundException(String arg0) {
        super(arg0);
    }
    
}
