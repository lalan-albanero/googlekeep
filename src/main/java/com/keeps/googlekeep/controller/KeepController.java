package com.keeps.googlekeep.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.keeps.googlekeep.Exception.NotFoundException;
import com.keeps.googlekeep.Service.KeepService;
import com.keeps.googlekeep.model.Keep;
import java.util.logging.Logger;

@RestController
public class KeepController {
    static final Logger logger = Logger.getLogger(KeepController.class.getName());

    @Autowired
    KeepService keepService;

    @PostMapping("/keeps")
    ResponseEntity<?> addKeep(@Validated @RequestBody Keep k1) {
        logger.info("Controller :-In addkeep method!!!");
        return ResponseEntity.status(HttpStatus.CREATED).body(keepService.addKeeps(k1));
    }

    @GetMapping("/keeps")
    ResponseEntity<?> getAllKeeps() throws NotFoundException {
        logger.info("Controller :- In getAllKeeps method!!!");
        return ResponseEntity.ok(keepService.getKeeps());
    }

    @GetMapping("/keeps/{id}")
    ResponseEntity<?> getKeepById(@PathVariable("id") String id) throws NotFoundException {
        logger.info("Controller :- In getKeepById method!!!");
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(keepService.getKeepById(id));
    }

    @PutMapping("/keeps/{id}")
    ResponseEntity<?> updateKeepById(@PathVariable("id") String id, @RequestBody Keep k1) {
        logger.info("Controller :- In updateKeepById method!!!");
        return ResponseEntity.ok(keepService.updateKeepById(id, k1));
    }

    @DeleteMapping("/keeps/{id}")
    ResponseEntity<?> deleteKeepById(@PathVariable("id") String id) {
        logger.info("Controller :- In deleteKeepById method!!!");
        return ResponseEntity.ok(keepService.deleteById(id));
    }

}
